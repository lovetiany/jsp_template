#jsp_template

jsp template 是参考了  rapid_jsp_extends tags 实现所做的一个 jsp 模版。

## 目的
- 使jsp可以实现类似"类"的继承关系,并不限继承层次
- 页面布局,父jsp页面定义好布局,子jsp可以重定义布局中的部分内容 

## demo

### example1

template.jsp 如下：
```
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tld/template.tld" prefix="t" %>
<html lang="en">
  <head>
  	<title><t:page name="title" value="模版title" />-网站名称</title>
    <%@include file="/jsp/struct/head_include.jsp" %>
    <t:page name="head_custom_include"></t:page>
  </head>

  <body>
   	<jsp:include page="/jsp/struct/navbar.jsp"></jsp:include>

    <div class="container">
		<t:page name="contianer">
			<h2>容器内容</h2>
		</t:page>	
    </div> 
    <!-- /container -->

	
	<%@include file="/jsp/struct/foot_include.jsp" %>
	<t:page name="foot_custom_include"></t:page>
   
  </body>
</html>
```

home.jsp

```
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tld/template.tld" prefix="t" %>
<t:template uri="template.jsp">
	<t:override name="title" value="首页" />
	<t:override name="contianer" >
		<!-- Main component for a primary marketing message or call to action -->
      <div class="jumbotron">
        <h1>Navbar example</h1>
        <p>This example is a quick exercise to illustrate how the default, static and fixed to top navbar work. It includes the responsive CSS and HTML, so it also adapts to your viewport and device.</p>
        <p>To see the difference between static and fixed top navbars, just scroll.</p>
        <p>
          <a class="btn btn-lg btn-primary" href="../../components/#navbar" role="button">View navbar docs &raquo;</a>
        </p>
      </div>
	</t:override>
	<t:override name="foot_custom_include">
		<script src="/js/home.js"></script>
	</t:override>
</t:template>
```

在 home.jsp，覆盖了template.jsp 的title，contianer，foot_custom_include 三个模块，其他的和模板一致

## 不足之处

标签 template 不能嵌套，此问题暂时没有想到好的解决方式。如下方式的调用，暂还不支持：
```
<t:template uri="template1">
  <t:override name="page1" />
  <t:template uri="template2">
    <t:override name="page2" />
  </t:template>
</t:template>
```