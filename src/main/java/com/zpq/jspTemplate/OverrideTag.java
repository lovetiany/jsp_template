package com.zpq.jspTemplate;

import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

public class OverrideTag extends BodyTagSupport {

    private static final long serialVersionUID = 1L;
    private String name;
    private String  value;

    @Override
    public int doStartTag() throws JspException {
        return EVAL_BODY_BUFFERED;
    }

    @Override
    public int doEndTag() throws JspException {
        @SuppressWarnings("unchecked")
        Map<String, Object> overrideMap = (Map<String, Object>) pageContext.getAttribute(
                Utils.getTemplateParamName("overrideMap"), PageContext.REQUEST_SCOPE);
        
        if (value != null && value.length() > 0) {
        	 overrideMap.put(Utils.getTemplateParamName(name), value);
        	 return EVAL_PAGE;
		}
        
        BodyContent bodyContent = getBodyContent();
        overrideMap.put(Utils.getTemplateParamName(name), bodyContent.getString());
        return EVAL_PAGE;
    }
    
    @Override
    public void release() {
        super.release();
        name = null;
        value = null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
