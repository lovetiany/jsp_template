package com.zpq.jspTemplate;

public class Utils {
    
    public static String TEMPLATE_PRIFIX = "__jsp_template__";
    public static int TEMPLATE_PRIFIX_LENGTH = TEMPLATE_PRIFIX.length();
    
    static String getTemplateParamName(String name) {
        return TEMPLATE_PRIFIX + name;
    }
}
