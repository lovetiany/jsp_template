<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="/META-INF/tld/template.tld" prefix="t" %>
<html lang="en">
  <head>
  	<title><t:page name="title" value="模版title" />-网站名称</title>
    <%@include file="/jsp/struct/head_include.jsp" %>
    <t:page name="head_custom_include"></t:page>
  </head>

  <body>
   	<jsp:include page="/jsp/struct/navbar.jsp"></jsp:include>

    <div class="container">
		<t:page name="contianer">
			<h2>容器内容</h2>
		</t:page>	
    </div> 
    <!-- /container -->

	
	<%@include file="/jsp/struct/foot_include.jsp" %>
	<t:page name="foot_custom_include"></t:page>
   
  </body>
</html>