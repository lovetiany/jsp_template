<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="/META-INF/tld/template.tld" prefix="t" %>
<t:template uri="template.jsp">
	<t:override name="title" value="首页" />
	<t:override name="contianer" >
		<!-- Main component for a primary marketing message or call to action -->
      <div class="jumbotron">
        <h1>Navbar example</h1>
        <p>This example is a quick exercise to illustrate how the default, static and fixed to top navbar work. It includes the responsive CSS and HTML, so it also adapts to your viewport and device.</p>
        <p>To see the difference between static and fixed top navbars, just scroll.</p>
        <p>
          <a class="btn btn-lg btn-primary" href="../../components/#navbar" role="button">View navbar docs &raquo;</a>
        </p>
      </div>
	</t:override>
	<t:override name="foot_custom_include">
		<script src="/js/home.js"></script>
	</t:override>
</t:template>